package productions.moo.engine;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import productions.moo.engine.render.animation.AnimatedSprite;
import productions.moo.engine.render.Sprite;
import productions.moo.engine.render.animation.AnimationInfo;
import productions.moo.engine.render.animation.MultiAnimationSprite;

public class AssetLoader
{
    private static AssetLoader _instance;

    private Map<String, Sprite> _assets = new HashMap<>();
    private AssetManager _manager;

    private AssetLoader() { }

    public static AssetLoader getInstance()
    {
        if(_instance == null)
        {
            _instance = new AssetLoader();
        }

        return _instance;
    }

    public Sprite getAsset(String name)
    {
        return _assets.get(name);
    }

    public void loadAssets(Context context)
    {
        try
        {
            _manager = context.getAssets();
            String[] files = _manager.list("");

            for(String file : files)
            {
                if(file.endsWith(".json"))
                {
                    loadAsset(file, context);
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void loadAsset(String fileName, Context context)
    {
        try {
            String jsonStr = readFileContents(fileName);
            JSONObject json = new JSONObject(jsonStr);

            String name = json.getString("name");

            String imageFile = json.getString("image");
            Bitmap image = loadImage(imageFile);

            if(json.has("animations"))
            {
                JSONArray animations = json.getJSONArray("animations");
                Map<String, AnimationInfo> animationMap = new HashMap<>();
                String defaultAnimation = null;

                for(int i = 0; i < animations.length(); i++)
                {
                    JSONObject animation = animations.getJSONObject(i);
                    AnimationInfo info = parseAnimation(animation);
                    String animationName = animation.getString("name");
                    animationMap.put(animationName, info);

                    if(defaultAnimation == null)
                    {
                        defaultAnimation = animationName;
                    }
                }

                _assets.put(name, new MultiAnimationSprite(context, image, animationMap, defaultAnimation));
            }
            else if(json.has("framePositions"))
            {
                AnimationInfo animInfo = parseAnimation(json);
                _assets.put(name, new AnimatedSprite(context, image, animInfo));
            }
            else
            {
                _assets.put(name, new Sprite(context, image));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String readFileContents(String fileName) throws IOException
    {
        Scanner fileInput = new Scanner(_manager.open(fileName));
        StringBuilder fileContents = new StringBuilder();

        while(fileInput.hasNextLine())
        {
            fileContents.append(fileInput.nextLine());
        }

        return fileContents.toString();
    }

    private Bitmap loadImage(String fileName) throws IOException
    {
        InputStream stream = _manager.open(fileName);
        return BitmapFactory.decodeStream(stream);
    }

    private AnimationInfo parseAnimation(JSONObject animation) throws JSONException
    {
        int frameWidth = animation.getInt("frameWidth");
        int frameHeight = animation.getInt("frameHeight");
        int frameDuration = animation.getInt("frameDuration");

        JSONArray frames = animation.getJSONArray("framePositions");
        int[][] framePos = new int[frames.length()][2];

        for (int i = 0; i < frames.length(); i++) {
            JSONArray frame = frames.getJSONArray(i);
            framePos[i][0] = frame.getInt(0);
            framePos[i][1] = frame.getInt(1);
        }

        return new AnimationInfo(frameWidth, frameHeight, frameDuration, framePos);
    }
}
