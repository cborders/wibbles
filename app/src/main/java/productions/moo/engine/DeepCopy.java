package productions.moo.engine;

/**
 * Created by cborders on 10/5/17.
 */

public interface DeepCopy<T>
{
    T deepCopy();
}
