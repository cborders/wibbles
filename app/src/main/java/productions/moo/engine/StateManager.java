package productions.moo.engine;

public class StateManager<T extends DeepCopy<T>>
{
    private T _previousState, _currentState;
    private final Object MUTEX = new Object();

    public StateManager(T initalState)
    {
        synchronized (MUTEX)
        {
            _previousState = initalState;
            _currentState = initalState.deepCopy();
        }
    }

    public T getCurrentSimState()
    {
        return _currentState;
    }

    public T getPreviousSimState()
    {
        return _previousState;
    }

    public T getCurrentRenderState()
    {
        synchronized (MUTEX)
        {
            return _previousState.deepCopy();
        }
    }

    public void swapSimBuffers()
    {
        synchronized (MUTEX)
        {
            T temp = _previousState;
            _previousState = _currentState;
            _currentState = temp;
        }
    }
}
