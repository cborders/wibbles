package productions.moo.engine.render;

import android.graphics.Canvas;
import android.graphics.Paint;

public interface Drawable
{
    void draw(Canvas canvas, Paint paint, long deltaTime);
}
