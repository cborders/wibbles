package productions.moo.engine.render;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;

import productions.moo.engine.DeepCopy;

public class Sprite implements Drawable, DeepCopy<Sprite>
{
    protected Context _context;
    protected Bitmap _bitmap;
    protected int _imageWidth, _imageHeight;
    protected int _posX, _posY;
    protected Rect _source, _dest;
    protected boolean _flippedHorizontal, _flippedVertical;

    public Sprite(Context context, Bitmap bitmap)
    {
        _context = context;
        _bitmap = bitmap;
        _imageWidth = _bitmap.getWidth();
        _imageHeight = _bitmap.getHeight();

        _source = new Rect(0, 0, _imageWidth, _imageHeight);
        _dest = new Rect(0, 0, _imageWidth, _imageHeight);

        setPosition(0, 0);
    }

    public void setPosition(int x, int y)
    {
        _posX = x;
        _posY = y;
    }

    public int getPosX()
    {
        return _posX;
    }

    public int getPosY()
    {
        return _posY;
    }

    public void flipHorizontal()
    {
        _flippedHorizontal = !_flippedHorizontal;
    }

    public void flipVertical()
    {
        _flippedVertical = !_flippedVertical;
    }

    @Override
    public void draw(Canvas canvas, Paint paint, long deltaTime)
    {
        canvas.save();
        canvas.translate(_posX, _posY);
        if(_flippedHorizontal)
        {
            canvas.translate(_imageWidth, 0);
            canvas.scale(-1, 1);
        }
        if(_flippedVertical)
        {
            canvas.translate(0, _imageHeight);
            canvas.scale(1, -1);
        }
        canvas.drawBitmap(_bitmap, _source, _dest, paint);
        canvas.restore();
    }

    @Override
    public Sprite deepCopy() {
        return new Sprite(_context, _bitmap);
    }
}
