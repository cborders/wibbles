package productions.moo.engine.render.animation;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

import productions.moo.engine.render.Sprite;

public class AnimatedSprite extends Sprite
{
    protected AnimationInfo _animation;
    protected int _currentFrame;
    protected long _startTime;
    protected int _spriteSheetWidth, _spriteSheetHeight;

    public AnimatedSprite(Context context, Bitmap bitmap, AnimationInfo animation)
    {
        super(context, bitmap);
        _animation = animation;

        // Keep track of the size of the whole spritesheet
        _spriteSheetWidth = _imageWidth;
        _spriteSheetHeight = _imageHeight;

        updateSourceRect();
        updateFrame();
        setPosition(0, 0);
    }

    @Override
    public void draw(Canvas canvas, Paint paint, long deltaTime)
    {
        _startTime += deltaTime;
        if(_startTime >= _animation.frameDuration) {
            // TODO: This doesn't skip more than one frame. Is that bad?
            _startTime -= _animation.frameDuration;
            _currentFrame = (_currentFrame + 1) % _animation.frameCount;
            updateFrame();
        }

        super.draw(canvas, paint, deltaTime);
    }

    @Override
    public void setPosition(int x, int y)
    {
        _posX = x;
        _posY = y;
    }

    @Override
    public Sprite deepCopy()
    {
        return new AnimatedSprite(_context, _bitmap, _animation);
    }

    protected void updateSourceRect()
    {
        // Trick the super class into thinking the image is just
        // the size of a single frame
        _imageWidth = _animation.frameWidth;
        _imageHeight = _animation.frameHeight;
        _dest.set(0, 0, _animation.frameWidth, _animation.frameHeight);
    }

    private void updateFrame()
    {
        final int startX = _animation.frameStart[_currentFrame][0];
        final int startY = _animation.frameStart[_currentFrame][1];
        _source.set(startX, startY, startX + _animation.frameWidth, startY + _animation.frameHeight);
    }
}
