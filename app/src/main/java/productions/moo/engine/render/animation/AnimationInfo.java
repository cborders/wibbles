package productions.moo.engine.render.animation;

public class AnimationInfo
{
    public int frameWidth, frameHeight;
    public int frameCount, frameDuration;
    public int[][] frameStart;

    public AnimationInfo(int frameWidth, int frameHeight, int frameDuration, int[][] frameStart)
    {
        this.frameWidth = frameWidth;
        this.frameHeight = frameHeight;
        this.frameCount = frameStart.length;
        this.frameDuration = frameDuration;
        this.frameStart = frameStart;
    }
}
