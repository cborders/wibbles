package productions.moo.engine.render.animation;

import android.content.Context;
import android.graphics.Bitmap;

import java.util.Map;

import productions.moo.engine.render.Sprite;

public class MultiAnimationSprite extends AnimatedSprite
{
    private Map<String, AnimationInfo> _animations;
    private String _defaultAnimation;

    public MultiAnimationSprite(Context context, Bitmap bitmap, Map<String, AnimationInfo> animations, String defaultAnimation)
    {
        super(context, bitmap, animations.get(defaultAnimation));
        _animations = animations;
        _defaultAnimation = defaultAnimation;
    }

    public void playAnimation(String name)
    {
        if(_animations.containsKey(name))
        {
            _startTime = 0;
            _currentFrame = 0;

            _animation = _animations.get(name);
            updateSourceRect();
        }
    }

    @Override
    public Sprite deepCopy()
    {
        return new MultiAnimationSprite(_context, _bitmap, _animations, _defaultAnimation);
    }
}
