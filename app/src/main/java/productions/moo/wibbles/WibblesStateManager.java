package productions.moo.wibbles;

import productions.moo.engine.StateManager;
import productions.moo.wibbles.models.State;

public class WibblesStateManager extends StateManager<State>
{
    private static WibblesStateManager _instance;

    private WibblesStateManager()
    {
        super(new State());
    }

    public static WibblesStateManager getInstance()
    {
        if(_instance == null) {
            _instance = new WibblesStateManager();
        }

        return _instance;
    }
}
