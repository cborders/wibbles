package productions.moo.wibbles.activities;

import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import productions.moo.wibbles.R;
import productions.moo.wibbles.renderer.WibblesRenderer;

public class GameActivity extends AppCompatActivity implements SurfaceHolder.Callback
{
    private Thread _renderThread;
    private Runnable _drawRunnable = new Runnable() {
        @Override
        public void run() {
            while(_running)
            {
                draw();
                Thread.yield();
            }
        }
    };

    private SurfaceView _surface;
    private WibblesRenderer _renderer;

    private boolean _surfaceValid, _running = true;
    private int _width, _height;

    private float _dpi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        _renderer = new WibblesRenderer(this);
        _renderThread = new Thread(_drawRunnable);

        _surface = findViewById(R.id.surface_view);
        _surface.getHolder().addCallback(this);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        _running = true;
        _renderer.start();
        _renderThread.start();
    }

    @Override
    protected void onPause()
    {
        try
        {
            // TODO: This doesn't stop the thread and causes a crash
            // TODO: if the app goes to the background and comes back
            _running = false;
            _renderThread.join();
        }
        catch (InterruptedException ie)
        {
            ie.printStackTrace();
        }

        _renderer.stop();
        super.onPause();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        synchronized (this)
        {
            _surfaceValid = true;
            _dpi = getResources().getDisplayMetrics().density;
            _renderer.setDpi(_dpi);
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        synchronized (this)
        {
            _surfaceValid = false;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
        synchronized (this)
        {
            _width = width;
            _height = height;
            _renderer.canvasChanged(_width, _height);
        }
    }

    private void draw()
    {
        synchronized (this)
        {
            if(_surfaceValid)
            {
                final SurfaceHolder holder = _surface.getHolder();
                final Canvas canvas = holder.lockCanvas();
                if(canvas != null)
                {
                    _renderer.draw(canvas);
                    holder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }
}
