package productions.moo.wibbles.models;

import productions.moo.engine.DeepCopy;

public class State implements DeepCopy<State>
{
    // For now we'll just have one Wibble and one World
    public Wibble wibble;
    public World world;

    public State()
    {
        wibble = new Wibble();
        world = new World();
    }

    @Override
    public State deepCopy()
    {
        State other = new State();
        other.wibble = wibble.deepCopy();
        other.world = world.deepCopy();
        return other;
    }
}
