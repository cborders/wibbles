package productions.moo.wibbles.models;

import productions.moo.engine.DeepCopy;

public class Wibble implements DeepCopy<Wibble>
{
    public enum State
    {
        SLEEPING,
        RESTING,
        WORKING,
        PLAYING,
        EATING
    }

    public static final float MAX_HUNGER = 100;
    public double hunger = 0;
    private State _state = State.SLEEPING;

    public State getState()
    {
        return _state;
    }

    @Override
    public Wibble deepCopy() {
        Wibble other = new Wibble();
        other.hunger = hunger;
        other._state = _state;

        return other;
    }
}
