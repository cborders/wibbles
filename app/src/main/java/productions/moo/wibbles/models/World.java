package productions.moo.wibbles.models;

import java.util.ArrayList;
import java.util.List;

import productions.moo.engine.DeepCopy;

public class World implements DeepCopy<World>
{
    public double food;

    public World()
    {
        food = 20;
    }

    @Override
    public World deepCopy() {
        World other = new World();
        other.food = food;
        return other;
    }
}
