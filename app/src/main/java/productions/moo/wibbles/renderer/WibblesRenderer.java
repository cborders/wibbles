package productions.moo.wibbles.renderer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import productions.moo.engine.AssetLoader;
import productions.moo.engine.render.Sprite;
import productions.moo.engine.render.animation.MultiAnimationSprite;
import productions.moo.wibbles.WibblesStateManager;
import productions.moo.wibbles.models.State;
import productions.moo.wibbles.models.Wibble;
import productions.moo.wibbles.services.WibblesSimulationService;
import productions.moo.wibbles.services.connections.WibblesSimulationConnection;

public class WibblesRenderer
{
    private static final float TEXT_SIZE = 12;

    private final Context _context;
    private final WibblesSimulationConnection _simulationConnection;
    private final WibblesStateManager _stateManager;
    private final Paint _paint;

    private boolean _simulationIsBound;

    private int _width, _height;
    private float _dpi;
    private long _previousFrame;

    private Sprite _spinner, _mario, _marioLeft;
    private MultiAnimationSprite _tim;

    public WibblesRenderer(Context context)
    {
        _context = context;
        // TODO: Should we really do this here?
        _simulationConnection = new WibblesSimulationConnection();
        _stateManager = WibblesStateManager.getInstance();

        _paint = new Paint();
        _paint.setAntiAlias(true);
        _paint.setColor(Color.WHITE);
        _paint.setStyle(Paint.Style.FILL);
        _paint.setStrokeJoin(Paint.Join.ROUND);
        _paint.setStrokeWidth(10);

        _paint.setTextSize(TEXT_SIZE);
        _paint.setTextAlign(Paint.Align.CENTER);

        AssetLoader loader = AssetLoader.getInstance();
        loader.loadAssets(context);



        _mario = loader.getAsset("Mario");
        _mario.flipVertical();

        _marioLeft = _mario.deepCopy();
        _marioLeft.setPosition(0, 800);
        _marioLeft.flipHorizontal();

        _spinner = loader.getAsset("Spinner");
        _spinner.setPosition(400, 0);

        _tim = (MultiAnimationSprite)loader.getAsset("Tim");
        _tim.setPosition(800, 0);
        _tim.playAnimation("Run");

        _previousFrame = System.currentTimeMillis();
    }

    public void start()
    {
        if(_simulationConnection.getService() == null || !_simulationIsBound)
        {
            final Intent intent = new Intent(_context, WibblesSimulationService.class);
            _context.bindService(intent, _simulationConnection, Context.BIND_AUTO_CREATE);
            _simulationIsBound = true;
        }
    }

    public void stop()
    {
        if(_simulationConnection.getService() != null && _simulationIsBound)
        {
            _context.unbindService(_simulationConnection);
            _simulationIsBound = false;
        }
    }

    public void setDpi(float dpi)
    {
        _dpi = dpi;
        _paint.setTextSize(TEXT_SIZE * _dpi);
    }

    public void canvasChanged(int width, int height)
    {
        _width = width;
        _height = height;
    }

    public void draw(Canvas canvas)
    {
        if(_simulationConnection.getService() != null)
        {
            long currentFrame = System.currentTimeMillis();
            long deltaTime = currentFrame - _previousFrame;
            _previousFrame = currentFrame;

            State gameState = _stateManager.getCurrentRenderState();

            // Wipe background
            canvas.drawColor(Color.BLUE);

            // Draw Wibble
            _paint.setColor(Color.RED);

            int x = _width / 2;
            int y = _height / 2;
            int radius = (int)((Wibble.MAX_HUNGER - gameState.wibble.hunger) * 4);
            canvas.drawCircle(x, y, radius, _paint);

            y = 200;
            canvas.drawText(String.format("Food: %.2f", gameState.world.food), x, y, _paint);

            _spinner.draw(canvas, _paint, deltaTime);
            _mario.draw(canvas, _paint, deltaTime);
            _marioLeft.draw(canvas, _paint, deltaTime);
            _tim.draw(canvas, _paint, deltaTime);
        }
    }
}
