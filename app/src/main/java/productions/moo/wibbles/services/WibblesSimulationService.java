package productions.moo.wibbles.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Date;

import productions.moo.engine.StateManager;
import productions.moo.wibbles.WibblesStateManager;
import productions.moo.wibbles.models.State;
import productions.moo.wibbles.models.Wibble;

public class WibblesSimulationService extends Service
{
    private static final long SIMULATION_TIME_STEP = 10;

    private final WibblesSimulationBinder _binder;
    private final WibblesStateManager _stateManager;
    private final Handler _handler;
    private final SimulationFrame _simFrame;

    private Date _previousFrameTime;

    public WibblesSimulationService()
    {
        _binder = new WibblesSimulationBinder(this);
        _handler = new Handler();
        _stateManager = WibblesStateManager.getInstance();
        _simFrame = new SimulationFrame();
        _previousFrameTime = new Date();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return _binder;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        // Start running simulation
        _handler.post(_simFrame);
    }

    public static class WibblesSimulationBinder extends Binder
    {
        private WibblesSimulationService _service;

        public WibblesSimulationBinder(WibblesSimulationService service)
        {
            _service = service;
        }

        public WibblesSimulationService getService()
        {
            return _service;
        }
    }

    private class SimulationFrame implements Runnable
    {
        @Override
        public void run()
        {
            final Date now = new Date();
            final long deltaTime = now.getTime() - _previousFrameTime.getTime();

            State previous = _stateManager.getPreviousSimState();
            State current = _stateManager.getCurrentSimState();

            // Grow Hungry
            current.wibble.hunger = previous.wibble.hunger + (0.01 * deltaTime);

            // Eat if possible
            if(current.wibble.hunger >= Wibble.MAX_HUNGER && previous.world.food > 0)
            {
                double foodWanted = current.wibble.hunger < (deltaTime) ? current.wibble.hunger : (deltaTime);
                if(previous.world.food > foodWanted)
                {
                    current.world.food = previous.world.food - foodWanted;
                    current.wibble.hunger -= foodWanted;
                }
                else
                {
                    current.world.food = 0;
                    current.wibble.hunger -= previous.world.food;
                }
            }
            else
            {
                current.world.food = previous.world.food;
            }

            _previousFrameTime = now;
            _stateManager.swapSimBuffers();

            _handler.removeCallbacks(_simFrame);
            _handler.postDelayed(_simFrame, SIMULATION_TIME_STEP);
        }
    }
}
