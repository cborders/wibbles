package productions.moo.wibbles.services;

import android.graphics.Canvas;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.view.SurfaceHolder;

import productions.moo.wibbles.renderer.WibblesRenderer;

public class WibblesWallpaperService extends WallpaperService
{
    @Override
    public Engine onCreateEngine() {
        return new WibblesEngine();
    }

    private class WibblesEngine extends WallpaperService.Engine
    {
        private static final int FRAME_DELAY = 500;

        private final Handler _renderDriver = new Handler();
        private final WibblesRenderer _renderer = new WibblesRenderer(WibblesWallpaperService.this);
        private final Runnable _drawer = new Runnable()
        {
            @Override
            public void run() {
                draw();
            }
        };

        private boolean _isVisible, _surfaceValid;

        public WibblesEngine()
        {
            _renderDriver.post(_drawer);
        }

        @Override
        public void onVisibilityChanged(boolean visible)
        {
            super.onVisibilityChanged(visible);
            _isVisible = visible;

            if(_isVisible) { _renderer.start(); }
            else { _renderer.stop(); }
        }

        @Override
        public void onSurfaceCreated(SurfaceHolder holder)
        {
            synchronized (this)
            {
                super.onSurfaceCreated(holder);
                _surfaceValid = true;
            }
        }

        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder)
        {
            synchronized (this)
            {
                super.onSurfaceDestroyed(holder);
                _surfaceValid = false;
            }
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height)
        {
            synchronized (this) {
                super.onSurfaceChanged(holder, format, width, height);
                _renderer.canvasChanged(width, height);
            }
        }

        private void draw()
        {
            synchronized (this)
            {
                if(_surfaceValid)
                {
                    final SurfaceHolder holder = getSurfaceHolder();
                    final Canvas canvas = holder.lockCanvas();
                    if(canvas != null)
                    {
                        _renderer.draw(canvas);
                        holder.unlockCanvasAndPost(canvas);
                    }
                }
            }

            _renderDriver.removeCallbacks(_drawer);
            if(_isVisible)
            {
                _renderDriver.postDelayed(_drawer, FRAME_DELAY);
            }
        }
    }
}
