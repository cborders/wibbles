package productions.moo.wibbles.services.connections;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

import productions.moo.wibbles.services.WibblesSimulationService;
import productions.moo.wibbles.services.WibblesSimulationService.WibblesSimulationBinder;

public class WibblesSimulationConnection implements ServiceConnection {
    private WibblesSimulationService _service;

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        _service = ((WibblesSimulationBinder)service).getService();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        _service = null;
    }

    public WibblesSimulationService getService()
    {
        return _service;
    }
}
